// In this example, we center the map, and add a marker, using a LatLng object
// literal instead of a google.maps.LatLng object. LatLng object literals are
// a convenient way to add a LatLng coordinate and, in most cases, can be used
// in place of a google.maps.LatLng object.

var map;

function initialize() {
    var mapOptions = {
        zoom: 14,
        center: {
            lat: 35.689823,
            lng: 139.765194
        }
    };
    map = new google.maps.Map(document.getElementById('map-basic'),
        mapOptions);

    var marker = new google.maps.Marker({
        // The below line is equivalent to writing:
        // position: new google.maps.LatLng(-34.397, 150.644)
        position: {
            lat: 35.689823,
            lng: 139.765194
        },
        mapTypeControl: true,
        map: map
    });

    // You can use a LatLng literal in place of a google.maps.LatLng object when
    // creating the Marker object. Once the Marker object is instantiated, its
    // position will be available as a google.maps.LatLng object. In this case,
    // we retrieve the marker's position using the
    // google.maps.LatLng.getPosition() method.
    var infowindow = new google.maps.InfoWindow({
        content: '<p>Marker Location:' + marker.getPosition() + '</p>'
    });

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
    });
}

google.maps.event.addDomListener(window, 'load', initialize);

function initialize2() {

    var fenway = new google.maps.LatLng(35.689823, 139.765194);
    var mapOptions = {
        center: fenway,
        zoom: 20
    };
    var panoramaOptions = {
        position: fenway,
        pov: {
            heading: 210.765194,
            pitch: 25.689823
        }
    };
    var panorama = new google.maps.StreetViewPanorama(document.getElementById('map-street'), panoramaOptions);
}



google.maps.event.addDomListener(window, 'load', initialize2);


jQuery(document).ready(function($) {
    $win = $(window);

    scrollTo = {
        target: $('.site-navigation a,.btn-mail'),
        root: $('html, body'),
        init: function() {
            scrollTo.target.on('click', function() {
                href = $(this).attr('href');
                scrollTo.click($(href));
                $('.site-header').removeClass('active-menu');
                return false;
            });
        },
        click: function(element) {
            scrollTo.root.animate({
                scrollTop: element.offset().top - 30
            }, 500);
        }
    }

    scrollTo.init();

    var hash = window.location.hash;

    try {
        if (hash) {
            scrollTo.root.animate({
                scrollTop: jQuery(hash).offset().top - 30
            }, 500);
        }
    } catch (err) {}


    windowHeight = $(window).outerHeight();

    $(document).on("scroll", onScroll);



    $(window).scroll(function() {
        var top = $(this).scrollTop();
        height = $('.site-header').outerHeight();
        $('body').css('padding-top',0);

        if (top > height) {
            $('.site-header').addClass('active-fixed');
            $('body').css('padding-top',height);
        } else {
            $('.site-header').removeClass('active-fixed');
        }
    });

    sameHeight($('.service-item'));

    var resizeFunctions = _.debounce(function() {

        sameHeight($('.service-item'));

    }, 400);

    $('.faq-question').on('click',function(){
    	$this = $(this);
    	$this.parent('.faq-item').toggleClass('toggle-active');
    	$this.next('.faq-answer').slideToggle('500');
    });

    $('.mobile-trigger').on('click',function(){
    	$(this).toggleClass('open');
    	$('.site-header').toggleClass('active-menu');
    });

    $win.resize(resizeFunctions);

    $('.wpb_gmaps_widget, .sp_maps_container').on('click', function() {
        $(this).addClass('active-map'); // set the pointer events true on click
    });

    // you want to disable pointer events when the mouse leave the canvas area;

    $(".wpb_gmaps_widget, .sp_maps_container").mouseleave(function() {
        $(this).removeClass('active-map'); // set the pointer events to none when mouse leaves the map area
    });

    $('html').on('touchstart', function(e) {
        $('.wpb_gmaps_widget, .sp_maps_container').removeClass('active-map');
    });

    $(".wpb_gmaps_widget, .sp_maps_container").on('touchstart',function(e) {
        e.stopPropagation();
    });

});

function sameHeight(target) {
    highest = 0;
    target.css('min-height', highest);
    windowWidth = $(window).outerWidth();
    if (windowWidth >= 980) {
        target.each(function() {
            high = $(this).outerHeight();

            if (highest < high) {
                highest = high;
            }
        });

        target.css('min-height', highest);
    }
}


function onScroll(event) {
    var scrollPos = $(document).scrollTop();
    $('.site-navigation .nav a').each(function() {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        elementTop = refElement.position().top;
        if (elementTop - 50 <= scrollPos && elementTop - 50 + refElement.height() > scrollPos) {
            $('.main-nav .nav a').removeClass("active");
            currLink.addClass("active");
        } else {
            currLink.removeClass("active");
        }
    });
}