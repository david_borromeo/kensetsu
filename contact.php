<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>見積り依頼フォーム | 避難安全検証法</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" media="screen">

</head>

<?php
$subject = '避難安全検証法「見積り依頼フォーム」';
$emailTo = 'davidpborromeo@gmail.com';


if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $company_name = stripslashes(trim($_POST['company_name']));
    $full_name = stripslashes(trim($_POST['full_name']));
    $furigana = stripslashes(trim($_POST['furigana']));
    $email = stripslashes(trim($_POST['email']));
    $contact_no = stripslashes(trim($_POST['contact_no']));
    $question = stripslashes(trim($_POST['question']));
    

    $pattern = '/[\r\n]|Content-Type:|Bcc:|Cc:/i';

    if (preg_match($pattern, $surname) || preg_match($pattern, $email) || preg_match($pattern, $subject)) {
        die("Header injection detected");
    }

    $emailIsValid = filter_var($email, FILTER_VALIDATE_EMAIL);

    if($company_name && $furigana && $contact_no && $full_name  && $email && $emailIsValid){
        $subject = "$subject";
        $body = "
        <strong>会社名</strong>:<br>$company_name<br><br>
        <strong>お名前</strong>:<br>$full_name<br><br>
        <strong>ふりがな</strong>:<br>$furigana<br><br>
        <strong>メールアドレス</strong>:<br>$email<br><br>
        <strong>電話番号</strong>:<br>$contact_no<br><br>
        <strong>ご質問・ご要望がありましたらお書きください</strong>:<br>$question<br><br>    
        ";

        $headers  = "MIME-Version: 1.1" . PHP_EOL;
        $headers .= "Content-type: text/html; charset=utf-8" . PHP_EOL;
        $headers .= "Content-Transfer-Encoding: 8bit" . PHP_EOL;
        $headers .= "Date: " . date('r', $_SERVER['REQUEST_TIME']) . PHP_EOL;
        $headers .= "Message-ID: <" . $_SERVER['REQUEST_TIME'] . md5($_SERVER['REQUEST_TIME']) . '@' . $_SERVER['SERVER_NAME'] . '>' . PHP_EOL;
        $headers .= "From: " . "=?UTF-8?B?".base64_encode($surname)."?=" . "<$email>" . PHP_EOL;
        $headers .= "Return-Path: $emailTo" . PHP_EOL;
        $headers .= "Reply-To: $email" . PHP_EOL;
        $headers .= "X-Mailer: PHP/". phpversion() . PHP_EOL;
        $headers .= "X-Originating-IP: " . $_SERVER['SERVER_ADDR'] . PHP_EOL;

        mail($emailTo, "=?utf-8?B?".base64_encode($subject)."?=", $body, $headers);
        $emailSent = true;
    } else {
        $hasError = true;
    }
}
?>

<body>
    <?php if(!empty($emailSent)): ?>
        <div class="center">
            <div class="alert alert-success text-center">メッセージが正常に送信されました。お問い合わせありがとうございます。<br>
                This page will be redirected to form page after 10 seconds or click <a href="../index.html#contact">this</a> </div>
        </div>
    <?php else: ?>
        <?php if(!empty($hasError)): ?>
        <div class="center">
            <div class="alert alert-danger text-center">Message was not sent try again. Make sure all required field has a correct value<br>This page will be redirected to form page after 10 seconds or click <a href="../index.html#contact">this</a></div>
        </div>
        <?php endif; ?>
    <?php endif; ?>

    <script>
    setTimeout(function () {
       window.location.href= 'index.html#contact'; // the redirect goes here

    },10000);
    </script>

</body>
</html>